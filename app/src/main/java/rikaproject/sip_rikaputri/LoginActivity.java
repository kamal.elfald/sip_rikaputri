package rikaproject.sip_rikaputri;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    ConstraintLayout cs1;
    Handler handler = new Handler();
    EditText txtusername, txtpassword;
    String user, pass;
    Button btnlogin, btnsimpan, btnlupa;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            cs1.setVisibility(View.VISIBLE);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        cs1 = (ConstraintLayout)findViewById(R.id.cs1);
        handler.postDelayed(runnable, 500);
        txtusername = (EditText)findViewById(R.id.txtusername);
        txtpassword = (EditText)findViewById(R.id.txtpassword);
        btnlogin = (Button)findViewById(R.id.btnlogin);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()){
                    user = txtusername.getText().toString();
                    pass = txtpassword.getText().toString();
                    login(user,pass);
                }
            }
        });
    }



        private boolean isValid() {
            if (txtusername.getText().toString().equals("")) {
                txtusername.setError("username kosong");
                return false;
            }
            if (txtpassword.getText().toString().equals("")) {
                txtpassword.setError("password tidak boleh kosong");
                return false;
            }
            return true;
        }
            private void login(String user, String pass){
                if (user.equals("admin") && (pass.equals("1234"))) {
                    showAlert("Selamat", "anda benar");
                }else{
                    showAlertTutup();
                }
            }

    private void showAlertTutup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage("Tutup aplikasi ini")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }

        })
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showAlert(String title, String pesan) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(pesan)
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
