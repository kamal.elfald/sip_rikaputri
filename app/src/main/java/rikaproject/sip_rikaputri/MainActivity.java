package rikaproject.sip_rikaputri;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import view.DBarangActivity;
import view.DUserActivity;

public class MainActivity extends AppCompatActivity {
    ImageButton btnbarang, btnuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnbarang = (ImageButton) findViewById(R.id.btnbarang);
        btnbarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,
                        DBarangActivity.class));
            }
        });
        btnuser = (ImageButton) findViewById(R.id.btnuser);
        btnuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,
                        DUserActivity.class));
            }
        });
    }
}