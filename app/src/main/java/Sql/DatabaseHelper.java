package Sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import model.BarangClass;
import model.User;

public class DatabaseHelper extends SQLiteOpenHelper {
    //databaseVersion
    private static final int DATABASE_VERSION = 1;
    //database nama
    private static final String DATABASE_NAME = "penjualan_db";

    public DatabaseHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BarangClass.CREATE_TABLE);
        db.execSQL(User.CREATE_TABLE);
        //db.execSQL("drop table BarangClass");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + BarangClass.TABLE_NAME);
        db.execSQL(" DROP TABLE IF EXISTS " + User.TABLE_NAME);
        //create table again
        onCreate(db);
    }
    public long insertBarangClass(String nama_barang, int harga_beli, int harga_jual, int stok){
        //get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values =  new ContentValues();
        //id and timestamp will be inserted automatically
        //no need to add them
        values.put(BarangClass.COLUMN_Nama_Barang, nama_barang);
        values.put(BarangClass.COLUMN_Harga_Beli, harga_beli);
        values.put(BarangClass.COLUMN_Harga_Jual, harga_jual);
        values.put(BarangClass.COLUMN_Stok, stok);

        long id = db.insert(BarangClass.TABLE_NAME, null, values);

        db.close();

        return id;
    }
    public BarangClass getBarangClass (long id){
        //get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(BarangClass.TABLE_NAME,
                new String[]{BarangClass.COLUMN_ID, BarangClass.COLUMN_Nama_Barang, BarangClass.COLUMN_Harga_Beli,
                BarangClass.COLUMN_Harga_Jual, BarangClass.COLUMN_Stok,BarangClass.COLUMN_TIMESTAMP},
                BarangClass.COLUMN_ID + "=?",
                new String[] {String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        //prepare note object
        BarangClass barangClass = new BarangClass(
                cursor.getInt(cursor.getColumnIndex(BarangClass.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(BarangClass.COLUMN_Nama_Barang)),
                cursor.getInt(cursor.getColumnIndex(BarangClass.COLUMN_Harga_Beli)),
                cursor.getInt(cursor.getColumnIndex(BarangClass.COLUMN_Harga_Jual)),
                cursor.getInt(cursor.getColumnIndex(BarangClass.COLUMN_Stok)),
                cursor.getString(cursor.getColumnIndex(BarangClass.COLUMN_TIMESTAMP)));

        //close the db connection
        cursor.close();

        return barangClass;
    }

    public List<BarangClass> getAllBarangClass(){
        List<BarangClass> notes = new ArrayList<>();

        //select all query
        String selectQuery = "SELECT * FROM " + BarangClass.TABLE_NAME + " ORDER BY " + BarangClass.COLUMN_TIMESTAMP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        //lopping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do{
                BarangClass barangClass = new BarangClass();
                barangClass.setId(cursor.getInt(cursor.getColumnIndex(BarangClass.COLUMN_ID)));
                barangClass.setNama_barang(cursor.getString(cursor.getColumnIndex(BarangClass.COLUMN_Nama_Barang)));
                barangClass.setHarga_beli(cursor.getInt(cursor.getColumnIndex(BarangClass.COLUMN_Harga_Beli)));
                barangClass.setHarga_jual(cursor.getInt(cursor.getColumnIndex(BarangClass.COLUMN_Harga_Jual)));
                barangClass.setStok(cursor.getInt(cursor.getColumnIndex(BarangClass.COLUMN_Stok)));
                barangClass.setTimestamp(cursor.getString(cursor.getColumnIndex(BarangClass.COLUMN_TIMESTAMP)));

                notes.add(barangClass);
            }while (cursor.moveToNext());
    }
    //close db connection
    db.close();
        return notes;
}

    public  int getBarangClassCount (){
        String countQuery = "SELECT * FROM " + BarangClass.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public int updateBarangClass(BarangClass barangClass){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BarangClass.COLUMN_Nama_Barang, barangClass.getNama_barang());
        values.put(BarangClass.COLUMN_Harga_Beli, barangClass.getHarga_beli());
        values.put(BarangClass.COLUMN_Harga_Jual, barangClass.getHarga_jual());
        values.put(BarangClass.COLUMN_Stok,barangClass.getStok());

        //updateing row
        return db.update(BarangClass.TABLE_NAME, values,
                BarangClass.COLUMN_ID + " = ?",
                new String[]{String.valueOf(barangClass.getId())});
    }

    public void deleteBarangClass(BarangClass barangClass) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(BarangClass.TABLE_NAME, BarangClass.COLUMN_ID + " = ?",
                new String[]{String.valueOf(barangClass.getId())});
        db.close();
    }
    public long insertUser(String nama, String username, String password, String email){
        //get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values =  new ContentValues();
        //id and timestamp will be inserted automatically
        //no need to add them
        values.put(User.COLUMN_Nama, nama);
        values.put(User.COLUMN_Username, username);
        values.put(User.COLUMN_Password, password);
        values.put(User.COLUMN_Email, email);

        long id = db.insert(User.TABLE_NAME, null, values);

        db.close();

        return id;
    }
    public User getUser (long id){
        //get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(User.TABLE_NAME,
                new String[]{User.COLUMN_ID, User.COLUMN_Nama, User.COLUMN_Username,
                        User.COLUMN_Password, User.COLUMN_Email},
                User.COLUMN_ID + "=?",
                new String[] {String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        //prepare note object
        User user = new User(
                cursor.getInt(cursor.getColumnIndex(User.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(User.COLUMN_Nama)),
                cursor.getString(cursor.getColumnIndex(User.COLUMN_Username)),
                cursor.getString(cursor.getColumnIndex(User.COLUMN_Password)),
                cursor.getString(cursor.getColumnIndex(User.COLUMN_Email)));

        //close the db connection
        cursor.close();

        return user;
    }
    public List<User> getAllUser(){
        List<User> notes = new ArrayList<>();

        //select all query
        String selectQuery = "SELECT * FROM " + User.TABLE_NAME + " ORDER BY " + User.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        //lopping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do{
                User user = new User();
                user.setId(cursor.getInt(cursor.getColumnIndex(User.COLUMN_ID)));
                user.setNama(cursor.getString(cursor.getColumnIndex(User.COLUMN_Nama)));
                user.setUsername(cursor.getString(cursor.getColumnIndex(User.COLUMN_Username)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(User.COLUMN_Password)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(User.COLUMN_Email)));

                notes.add(user);
            }while (cursor.moveToNext());
        }
        //close db connection
        db.close();
        return notes;
    }
    public  int getUserCount (){
        String countQuery = "SELECT * FROM " + User.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public int updateUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(User.COLUMN_Nama, user.getNama());
        values.put(User.COLUMN_Username, user.getUsername());
        values.put(User.COLUMN_Password, user.getPassword());
        values.put(User.COLUMN_Email,user.getEmail());

        //updateing row
        return db.update(User.TABLE_NAME, values,
                User.COLUMN_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
    }

    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(User.TABLE_NAME, User.COLUMN_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }
}

