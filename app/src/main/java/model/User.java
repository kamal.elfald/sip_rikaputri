package model;

public class User {
    public static final String TABLE_NAME = "user";


    public static final String COLUMN_ID = "id";
    public static final String COLUMN_Nama = "nama";
    public static final String COLUMN_Username = "username";
    public static final String COLUMN_Password= "password";
    public static final String COLUMN_Email = "email";


    private int id;
    private String nama;
    private String username;
    private String password;
    private String email;
    public static final String CREATE_TABLE =
            "Create Table " + TABLE_NAME+ "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_Nama+ " TEXT,  "
                    + COLUMN_Username+ "  TEXT, "
                    + COLUMN_Password+ "  TEXT, "
                    + COLUMN_Email+ "    TEXT  "
                    +")";
    public User()
    {}

    public User(int id, String nama, String username, String password, String email) {
        this.id = id;
        this.nama = nama;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNama(String nama_lengkap) {this.nama = nama;}

    public void setUsername(String username) {this.username = username;}

    public void setPassword(String password) {this.password = password;}

    public void setEmail(String email) {this.email = email;}

    public int getId() {return id;}

    public String getNama() {return nama;}

    public String getUsername() {return username;}

    public String getPassword() {return password;}

    public String getEmail() {return this.email;}

}
