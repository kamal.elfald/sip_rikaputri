package view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Sql.DatabaseHelper;
import model.BarangClass;
import model.User;
import rikaproject.sip_rikaputri.R;
import utils.MyDividerItemDecoration;
import utils.RecyclerTouchListener;

public class DUserActivity extends AppCompatActivity {
    private DatabaseHelper db;
    private DataUserAdapter mAdapter;
    private List<User> userList = new ArrayList<>();

    private RecyclerView recyclerView;
    private TextView noUserView;
    private  FloatingActionButton fab;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duser);
        noUserView=(TextView) findViewById(R.id.noUserView);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_user);

        db = new DatabaseHelper(this);

        userList.addAll(db.getAllUser());

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                showUserDialog(false,
                        null,-1);
            }

        });
        mAdapter = new DataUserAdapter(this, userList);
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager((getApplicationContext()));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this,
                LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);
        toggleEmptyUser();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener(){
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {
                showActionDialog(position);
            }


        }));

    }
    private void showActionDialog(final int position) {
        CharSequence colors[] = new CharSequence[]{"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    showUserDialog(true, userList.get(position), position);
                } else {
                    deleteUser(position);
                }
            }


        });
        builder.show();
    }

    private void deleteUser(int position) {
        db.deleteUser(userList.get(position));
        userList.remove(position);
        mAdapter.notifyItemRemoved(position);
        toggleEmptyUser();
    }

    private void createUser(String nama, String username, String password, String email) {
        long id = db.insertUser(nama, username, password, email);
        User n = db.getUser(id);
        if (n != null) {
            userList.add(n);
            mAdapter.notifyDataSetChanged();
            toggleEmptyUser();
        }
    }

    private void updateUser(String nama, String username, String password, String email, int position) {
        User n = userList.get(position);
        n.setNama(nama);
        n.setUsername(username);
        n.setPassword(password);
        n.setEmail(email);
        db.updateUser(n);
        userList.set(position, n);
        mAdapter.notifyItemChanged(position);
        toggleEmptyUser();
    }

    private void showUserDialog(final boolean shouldUpadte, final User user, final int position) {
        LayoutInflater LayoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        View view = LayoutInflaterAndroid.inflate(R.layout.user_dialog, null);

        AlertDialog.Builder alertDioalogBuilderUseInput = new AlertDialog.Builder(DUserActivity.this);
        alertDioalogBuilderUseInput.setView(view);

        final EditText inputNama = (EditText) view.findViewById(R.id.nama);
        final EditText inputUsername = (EditText) view.findViewById(R.id.username);
        final EditText inputPassword = (EditText) view.findViewById(R.id.password);
        final EditText inputEmail = (EditText) view.findViewById(R.id.email);

        final TextView dialogTitle = (TextView) view.findViewById(R.id.dialog_title);
        dialogTitle.setText(!shouldUpadte ? "User Baru" : "Rdit Note");

        if (shouldUpadte && user != null) {
            inputNama.setText(user.getNama());
            inputUsername.setText(user.getUsername());
            inputPassword.setText(user.getPassword());
            inputEmail.setText(user.getEmail());
        }

        alertDioalogBuilderUseInput
                .setCancelable(false)
                .setPositiveButton(shouldUpadte ? "update" : "save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {

                            }
                        })
                .setNegativeButton("cancle",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int i) {
                                dialogBox.cancel();
                            }
                        });
        final AlertDialog alertDialog = alertDioalogBuilderUseInput.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(inputNama.getText().toString())) {
                    Toast.makeText(DUserActivity.this, "Enter Nama User!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }
                if (TextUtils.isEmpty(inputUsername.getText().toString())) {
                    Toast.makeText(DUserActivity.this, "Enter Username!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }
                if (TextUtils.isEmpty(inputPassword.getText().toString())) {
                    Toast.makeText(DUserActivity.this, "Enter Password!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }
                if (TextUtils.isEmpty(inputEmail.getText().toString())) {
                    Toast.makeText(DUserActivity.this, "Enter Email!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }
                if (shouldUpadte && user != null) {
                    updateUser(inputNama.getText().toString(),
                            inputUsername.getText().toString(),
                            inputPassword.getText().toString(),
                            inputEmail.getText().toString(), position);
                } else {
                    createUser(inputNama.getText().toString(),
                            inputUsername.getText().toString(),
                            inputPassword.getText().toString(),
                            inputEmail.getText().toString());
                }

            }

        });
    }
    private void toggleEmptyUser() {
        if (db.getBarangClassCount() > 0){
            noUserView.setVisibility(View.GONE);
            fab.setVisibility(View.VISIBLE);
        }else{
            noUserView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        }
    }
}