package view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import model.BarangClass;
import rikaproject.sip_rikaputri.R;

public class DataBarangAdapter extends RecyclerView.Adapter<DataBarangAdapter.DataBarangHolder> {
    Context context;
    List<BarangClass>data;
    public class DataBarangHolder extends RecyclerView.ViewHolder{
        TextView txtNama,txtStok,dot;
        public DataBarangHolder(View itemView){
            super(itemView);
            txtNama = (TextView) itemView.findViewById(R.id.txtnama_barang);
//            dot = (TextView) itemView.findViewById(R.id.dot);
            txtStok = (TextView) itemView.findViewById(R.id.stokbr);

        }
    }

    public DataBarangAdapter(Context context, List<BarangClass> data) {
        this.context = context;
        this.data = data;
    }
    @NonNull
    public DataBarangHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_databarang,parent, false);
        return new DataBarangHolder(v);
    }
    public void onBindViewHolder(@NonNull DataBarangHolder holder,int position){
        BarangClass barangClass = data.get(position);
        holder.txtNama.setText(barangClass.getNama_barang());
//        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.txtStok.setText(Integer.toString(barangClass.getStok()));
    }
    public int getItemCount(){return data.size();}
}
