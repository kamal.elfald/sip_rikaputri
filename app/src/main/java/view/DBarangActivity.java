package view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import Sql.DatabaseHelper;
import model.BarangClass;
import rikaproject.sip_rikaputri.R;
import utils.MyDividerItemDecoration;
import utils.RecyclerTouchListener;

public class DBarangActivity extends AppCompatActivity {
    private DatabaseHelper db;
    private DataBarangAdapter mAdapter;
    private List<BarangClass> barangClassList = new ArrayList<>();

    private RecyclerView recyclerView;
    private TextView noBarangView;
    private  FloatingActionButton fab;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbarang);
        noBarangView=(TextView) findViewById(R.id.noBarangView);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_barang);

        db = new DatabaseHelper(this);

        barangClassList.addAll(db.getAllBarangClass());
        Log.i("BARANG", "createBarang: "+new Gson().toJson(barangClassList));

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                showBarangClassDialog(false,
                        null,-1);
            }

        });
        mAdapter = new DataBarangAdapter(this, barangClassList);
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager((getApplicationContext()));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this,
                LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);
        toggleEmptyBarangClass();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener(){
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {
                showActionDialog(position);
            }


        }));

    }
            private void showActionDialog(final int position) {
                CharSequence colors[] = new CharSequence[]{"Edit", "Delete"};

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose option");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            showBarangClassDialog(true, barangClassList.get(position), position);
                        } else {
                            deleteBarangClass(position);
                        }
                    }


                });
                builder.show();
            }

            private void deleteBarangClass(int position) {
                db.deleteBarangClass(barangClassList.get(position));
                barangClassList.remove(position);
                mAdapter.notifyItemRemoved(position);
                toggleEmptyBarangClass();
            }

            private void createBarang(String nama, int harga_beli, int harga_jual, int stok) {
                long id = db.insertBarangClass(nama, harga_beli, harga_jual, stok);
                BarangClass n = db.getBarangClass(id);
                Log.i("BARANG", "createBarang: "+n.toString());
                if (n != null) {
                    barangClassList.add(n);
                    mAdapter.notifyDataSetChanged();
                    toggleEmptyBarangClass();
                }
            }

            private void updateBarang(String nama, int harga_beli, int harga_jual, int stok, int position) {
                BarangClass n = barangClassList.get(position);
                n.setNama_barang(nama);
                n.setHarga_beli(harga_beli);
                n.setHarga_jual(harga_jual);
                n.setStok(stok);
                db.updateBarangClass(n);
                barangClassList.set(position, n);
                mAdapter.notifyItemChanged(position);
                toggleEmptyBarangClass();
            }

            private void showBarangClassDialog(final boolean shouldUpadte, final BarangClass barangClass, final int position) {
                LayoutInflater LayoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
                View view = LayoutInflaterAndroid.inflate(R.layout.barang_dialog, null);

                AlertDialog.Builder alertDioalogBuilderUseInput = new AlertDialog.Builder(DBarangActivity.this);
                alertDioalogBuilderUseInput.setView(view);

                final EditText inputNama = (EditText) view.findViewById(R.id.nama_barang);
                final EditText inputHarga_Beli = (EditText) view.findViewById(R.id.harga_beli);
                final EditText inputHarga_jual = (EditText) view.findViewById(R.id.harga_jual);
                final EditText inputStok = (EditText) view.findViewById(R.id.stok);

                final TextView dialogTitle = (TextView) view.findViewById(R.id.dialog_title);
                dialogTitle.setText(!shouldUpadte ? "Barang Baru" : "Rdit Note");

                if (shouldUpadte && barangClass != null) {
                    inputNama.setText(barangClass.getNama_barang());
                    inputHarga_Beli.setText(Integer.toString(barangClass.getHarga_beli()));
                    inputHarga_jual.setText(Integer.toString(barangClass.getHarga_jual()));
                    inputStok.setText(Integer.toString(barangClass.getStok()));
                }

                alertDioalogBuilderUseInput
                        .setCancelable(false)
                        .setPositiveButton(shouldUpadte ? "update" : "save",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {

                                    }
                                })
                        .setNegativeButton("cancle",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int i) {
                                        dialogBox.cancel();
                                    }
                                });
                final AlertDialog alertDialog = alertDioalogBuilderUseInput.create();
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (TextUtils.isEmpty(inputNama.getText().toString())) {
                            Toast.makeText(DBarangActivity.this, "Enter Nama Barang!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (TextUtils.isEmpty(inputHarga_Beli.getText().toString())) {
                            Toast.makeText(DBarangActivity.this, "Enter Harga Beli!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (TextUtils.isEmpty(inputHarga_jual.getText().toString())) {
                            Toast.makeText(DBarangActivity.this, "Enter Harga Jual!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (TextUtils.isEmpty(inputStok.getText().toString())) {
                            Toast.makeText(DBarangActivity.this, "Enter Stok!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            alertDialog.dismiss();
                        }
                        if (shouldUpadte && barangClass != null) {
                            updateBarang(inputNama.getText().toString(),
                                    Integer.parseInt(inputHarga_Beli.getText().toString()),
                                    Integer.parseInt(inputHarga_jual.getText().toString()),
                                    Integer.parseInt(inputStok.getText().toString()), position);
                        } else {
                            createBarang(inputNama.getText().toString(),
                                    Integer.parseInt(inputHarga_Beli.getText().toString()),
                                    Integer.parseInt(inputHarga_jual.getText().toString()),
                                    Integer.parseInt(inputStok.getText().toString()));
                        }

                    }

                });
            }
    private void toggleEmptyBarangClass() {
        if (db.getBarangClassCount() > 0){
            noBarangView.setVisibility(View.GONE);
            fab.setVisibility(View.VISIBLE);
        }else{
            noBarangView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        }
    }
        }