package view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;



import model.User;
import rikaproject.sip_rikaputri.R;

public class DataUserAdapter extends RecyclerView.Adapter<DataUserAdapter.DataUserHolder> {
    Context context;
    List<User> data;
    public class DataUserHolder extends RecyclerView.ViewHolder{
        TextView txtNama,txtUsername,dot;
        public DataUserHolder(View itemView){
            super(itemView);
            txtNama = (TextView) itemView.findViewById(R.id.nama);
            dot = (TextView) itemView.findViewById(R.id.dot);
            txtUsername = (TextView) itemView.findViewById(R.id.username);

        }
    }
    public DataUserAdapter(Context context, List<User> data) {
        this.context = context;
        this.data = data;
    }
    public DataUserAdapter.DataUserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_datauser,parent, false);
        return new DataUserAdapter.DataUserHolder(v);
    }
    public void onBindViewHolder(@NonNull DataUserAdapter.DataUserHolder holder, int position){
        User user = data.get(position);
        holder.txtNama.setText(user.getNama());
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.txtUsername.setText(user.getUsername());
    }
    public int getItemCount(){return data.size();}

}
